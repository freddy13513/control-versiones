# Distribuciones

GitHub: Una plataforma en la nube que utiliza Git para el control de versiones. Proporciona funciones adicionales como seguimiento de problemas, colaboración y despliegue continuo.

GitLab: Similar a GitHub, GitLab es una plataforma que proporciona servicios de control de versiones basados en Git junto con herramientas adicionales para la gestión del ciclo de vida del software.

Bitbucket: Ofrece servicios de control de versiones basados en Git y Mercurial. Es conocido por su integración con otras herramientas de desarrollo de Atlassian, como Jira.

![image](/img/distribuciones.png)