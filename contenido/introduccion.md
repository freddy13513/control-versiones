# Introduccion
Git es lo que se denomina un sistema de control de versiones distribuido. Está escrito en lenguaje C.

Un sistema de control de versiones permite llevar un registro de las modificaciones realizadas sobre un conjunto de archivos y directorios en cuanto a creación, modificación o eliminación.

El objetivo de dicho registro es poder revertir en cualquier momento las modificaciones realizadas tiempo atrás, compararlas con la versión actual o incluso mantener varias versiones del proyecto simultáneamente.

Asimismo, es posible clonar el proyecto completo en otra ubicación o sincronizar el repositorio local con otro remoto mediante herramientas como GitHub.

![image](/img/introduccion.png)