# control-versiones

![image](/img/versiones.jpg)

## Indice
1. [Introduccion](/contenido/introduccion.md)
2. [Caracteristicas](/contenido/caracteristicas.md)
3. [Distribuciones](/contenido/distribuciones.md)

## Referencias
[JGR](https://www.jairogarciarincon.com/clase/control-de-versiones-con-github/introduccion-a-git)

[Atlassian](https://www.atlassian.com/es/git/tutorials/what-is-version-control)

## Autores

Jefe de proyecto - [Freddy Lopez](https://gitlab.com/freddy13513)

Colaborador - [Daniel Tenorio](https://gitlab.com/DanielTenorioF)